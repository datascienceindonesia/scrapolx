# -*- coding: utf-8 -*-
import scrapy
# import time
from scrapy.selector import Selector
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapolx.items import ScrapolxItem


class ScrappropertiSpider(CrawlSpider):
	name = "scrapproperti"
	allowed_domains = ['olx.co.id']
	start_urls = [
		'http://olx.co.id/properti/rumah/yogyakarta-di/'
		# 'http://olx.co.id/properti/apartment/yogyakarta-di/',
		# 'http://olx.co.id/properti/indekos/yogyakarta-di/'
		# 'http://olx.co.id/properti/bangunan-komersil/yogyakarta-di/',
		# 'http://olx.co.id/properti/tanah/yogyakarta-di/'
	]

	rules = (
		Rule(LinkExtractor(allow=(), restrict_css=('.pageNextPrev',)),
			callback="parse_item",
			follow=True),)

	def parse_item(self, response):
		print('Processing.. ' + response.url)
		item_links = response.css('.large > .detailsLink::attr(href)').extract()
		for a in item_links:
			yield scrapy.Request(a, callback=self.parse_detail_page)

	def parse_detail_page(self, response):
		# print('Processing.. ' + response.url)
		sel = Selector(response)
		title = response.css('h1::text').extract()[0].strip()
		price = response.css('.pricelabel > strong span::text').extract()[0]
		description = response.xpath('//span[@itemprop="description"]/text()').extract()
		spesifications = response.css('ul.spesifikasi li').extract()
		facilities = response.css('ul.fasilitas li a::text').extract()

		address = response.css('ul.address div::text').extract()
		if len(address) > 0:
			address = response.css('ul.address div::text').extract()[0].strip()
		else:
			address = ""

		location = response.css('ul.lokasi a::text').extract()
		if len(location) > 0:
			location = response.css('ul.lokasi a::text').extract()[0].strip()
		else:
			location = ""

		viewed = response.css('#offerbottombar strong::text').extract()
		if len(viewed) > 0:
			viewed = response.css('#offerbottombar strong::text').extract()[0].strip()
		else:
			viewed = ""

		author = response.css('p.userdetails span.xx-large::text').extract()
		if len(author) > 0:
			author = response.css('p.userdetails span.xx-large::text').extract()[0].strip()
		else:
			author = ""

		surface_area = ""
		building_area = ""
		bedroom = ""
		bathroom = ""
		sertification = ""

		for spesification in spesifications:
			sel = Selector(text=spesification)

			listSpesification = sel.xpath('//span/text()').extract()[0]
			valueOfListSpesification = sel.css('li ::text').extract()[2].replace(":", "")

			if listSpesification == "Kamar mandi" or listSpesification == "Kamar tidur" or listSpesification == "Sertifikasi":
				valueOfListSpesification = sel.css('li ::text').extract()[3]

			if (listSpesification == "Luas tanah"):
				surface_area = valueOfListSpesification

			if (listSpesification == "Luas bangunan"):
				building_area = valueOfListSpesification

			if (listSpesification == "Kamar mandi"):
				bathroom = valueOfListSpesification 

			if (listSpesification == "Sertifikasi"):
				sertification = valueOfListSpesification 

			if (listSpesification == "Kamar tidur"):
				bedroom = valueOfListSpesification 

		fclty = ""
		for facility in facilities:
			sel = Selector(text=facility)

			fclty += "".join(facility.split()) + ', '

		fclty = fclty[:-2]

		item = ScrapolxItem()
		item['title'] = title
		item['price'] = price
		item['url'] = response.url
		item['surface_area'] = "".join(surface_area.split())
		item['building_area'] = "".join(building_area.split())
		item['bedroom'] = "".join(bedroom.split())
		item['bathroom'] = "".join(bathroom.split())
		# item['electricity'] = scrapy.Field()
		# item['description'] = description
		item['sertification'] = sertification
		item['facilities'] = fclty
		item['location'] = location
		item['address'] = address
		item['viewed'] = viewed
		item['author'] = author
		yield item