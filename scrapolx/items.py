# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ScrapolxItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    url = scrapy.Field()
    surface_area = scrapy.Field()
    building_area = scrapy.Field()
    bedroom = scrapy.Field()
    bathroom = scrapy.Field()
    electricity = scrapy.Field()
    description = scrapy.Field()
    image = scrapy.Field()
    category = scrapy.Field()
    sertification = scrapy.Field()
    facilities = scrapy.Field()
    location = scrapy.Field()
    address = scrapy.Field()
    viewed = scrapy.Field()
    author = scrapy.Field()